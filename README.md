# Light Intensity depended Automatic Light

A simple AVR micro-controller based project that turns on and off lights depending upon the light intensity. This project was designed in 2014 to automatically turn ON and OFF street lights based upon the amount of natural light. At that time switching of street lights were mostly done manually by a person that resulted in occasional incidents like lights being ON even when there is sufficient natural light present or vice versa. This project was simply designed to provide an inexpensive solution to the aforementioned problem.
    

## Simulating

To simulate, you must install all the softwares listed in the prerequisite section and then following the "How to simulate" section 

### Prerequisites

 * Proteus Design Suite


### How-to-simulate
 
 * Clone the repo in your local directory
 * Start Proteus Design Suite
 * In Proteus, open either "auto.DSN" or "auto2.DSN"  
 * When the design opens, double-click on the AVR microcontroller that opens the "edit component" pop-up
 * From the edit component, add the location of the main.hex file (located in the folder called program) in the program file field and click OK.
 * Press the play button from the bottom left menu. Voila!!
 * Once started, you can change the light intensity of the light source to imitate the increase and decrease of natural light.
 
## Changing the program code

  * To change the program code, you may use AVR Studio/WinAVR to open the main.c file (located in the folder called program) and change.
  * Once changed, compile it into hex file using AVR Studio/ WinAVR, and load it again to the Atmega8 following the steps from "How-to-simulate"
  * If you are using WinAVR you may use the provided Makefile in the folder called program to quickly configure the compiler. 
  
## Hardware Implementation 

  * You may have to experimentally obtain a suitable value for the variable "light_lim" according to the environment you intend to use the device.
  * In this project design, simple LED is used as light which are directly controlled by the micro-controller. In actual implementation, powerful lights may need to be used. In that case, a suitable driver IC or a relay circuit need to be used. 
  * Before printing the circuit in board, experimentally make sure the hardware implementation work because the real world is not always similar to simulation world. We had to do several tweaks to make it work in the real world.  
  * The main.hex file must me burned (not literally) in the microcontroller using AVR programmer or some universal programmer that supports it.
  

 
## Author

* **[Abhijit Das](https://www.linkedin.com/in/abhijit-das-jd/)**


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Gratitude to all my teachers, mentors, book writers and online article writers who provided me with the knowledge required to do the project. 
